
function generalAjax(id,option){
    $.ajax({
        method: "POST",
        url: "PreguntaCtr",
        data: {id:id, option: option , enunciadoCtx:GetValueToInputText("enunciadoCtx"), valorCtx: GetValueToInputText("valorCtx") },
        success: function(dataTbl){ 
            ressetControls();
            $("#preguntasTbl").html(dataTbl);
        }  
    });
    
}

function setDataInControls(id,enunciado,valor){
    
    SetValueToInputText("enunciadoCtx",enunciado);
    SetValueToInputText("valorCtx",valor);
    $("#modificarBtn").show();
    $("#registrarBtn").hide();
    
    $("#modificarBtn").click(function (){
        generalAjax(id,2);
    });
}

function ressetControls(){
    SetValueToInputText("enunciadoCtx","");
    SetValueToInputText("valorCtx","");
    $("#modificarBtn").hide();
    $("#registrarBtn").show();    
}


$( document ).ready(function() {
    generalAjax(0,4);
});
