<%-- 
    Document   : studentsView
    Created on : 31-may-2021, 23:10:14
    Author     : Andres Angarita<andres.angarita96@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>students</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar navbar-light" style="background-color: #e3f2fd;">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                <a class="nav-link" href="#">Home</a>
                <a class="nav-link active" aria-current="page" href="#">Students</a>
                <a class="nav-link" href="#">courses</a>
                <a class="nav-link" href="#">Grades</a>
                </div>
            </div>
        </div>    
    </nav>
    <div class="container">
        <div class="row">
          <div class="col-2"></div>
          <div class="col-6">
            <div class="container">
                <h3>Nuevo estudiante</h3>
                <!-- ${pageContext.request.contextPath}/PreguntaCtr-->
                <form action="../StudentServlet" method="post" >
                    <div class="form-group row">
                        <label for="nameCtx" class="col-sm-1-12 col-form-label"></label>
                        <div class="col-sm-1-12">
                            <input type="text" class="form-control" name="nameCtx" id="nameCtx" placeholder="Nombre">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lastNameCtx" class="col-sm-1-12 col-form-label"></label>
                        <div class="col-sm-1-12">
                            <input type="text" class="form-control" name="lastNameCtx" id="lastNameCtx" placeholder="Apellido">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="birthdayCtx" class="col-sm-1-12 col-form-label"></label>
                        <div class="col-sm-1-12">
                            <input type="date" class="form-control" name="birthdayCtx" id="birthdayCtx" placeholder="Fecha nto">
                        </div>
                    </div>
                    <div style="margin-top: 5px;"></div>
                    <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Crear</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <div class="col-4"></div>
        </div>
      </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>