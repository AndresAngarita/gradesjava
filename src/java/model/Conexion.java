
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author Andres Angarita<andres.angarita96@gmail.com>
 */
public class Conexion {
    private static String user = "root";
    private static String pass = "";
    private static String url  = "jdbc:mysql://localhost/notas_db";
    private static Connection con = null;
    
    public static Connection getConexion(){
        if (con == null){
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                con = DriverManager.getConnection(url,user,pass);
            } catch (ClassNotFoundException ex) {
                System.out.println("Oops no se encontró el driver!");
            } catch (SQLException ex) {
                System.out.println("Oops no se pudo conectar!");
            }
        }
            return con;
    }
    
    public static void desconectar(){
        if(con != null){
            con = null;
        }
    }
}
