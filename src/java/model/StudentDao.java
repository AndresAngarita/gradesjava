package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Andres Angarita<andres.angarita96@gmail.com>
 */
public class StudentDao {
    
    private Connection con;
    private Statement st;
    private ResultSet rs;
    
    public StudentDao() {
        con = null;
        st = null;
        rs = null;
    }
    public Student getStudent(int id){
        Student student = null;
        String query = "Select * from students where id = "+id;
        
        try {
            Conexion.getConexion();
            st = con.createStatement();
            rs = st.executeQuery(query);
            
            if(rs.next()){
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setLastName(rs.getString("lastname"));
                student.setBirthday(rs.getString("birthday"));
            }
            st.close();
            Conexion.desconectar();
        } catch (SQLException e) {
            System.out.println("No se pudo conectar");
        }
        return student;
    }
    
    public ArrayList<Student> getStudents(){
        ArrayList<Student> students = new ArrayList<Student>();
        String query = "select * from students";
        
        try {
            con = Conexion.getConexion();
            st = con.createStatement();
            rs = st.executeQuery(query);
            
            while(rs.next()){
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setLastName(rs.getString("lastname"));
                student.setBirthday(rs.getString("birthday"));
                
                students.add(student);
            }
            
            st.close();
            Conexion.desconectar();
        } catch (SQLException e) {
            System.out.println("no se pudo realizar la consulta");
        }
        return students;
    }
    
     public void insertStudent(Student student){
        try {
            con = Conexion.getConexion();
            st = con.createStatement();
            String insert = "INSERT INTO students (id,name,lastname,birthday)VALUES("+student.getId()+",'"+student.getName()+"','"+student.getLastName()+"','"+student.getBirthday()+"')";
            st.executeUpdate(insert);
            st.close();
            Conexion.desconectar();
        } catch (SQLException ex) {
            System.out.println("no se pudo realizar la insercion");
        }
    }
}
