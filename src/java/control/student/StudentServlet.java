/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.student;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Student;
import model.StudentDao;

/**
 *
 * @author Andres Angarita<andres.angarita96@gmail.com>
 */
@WebServlet(name = "StudentServlet", urlPatterns = {"/StudentServlet"})
public class StudentServlet extends HttpServlet {
    StudentDao studentDao = new StudentDao();
    Student student = new Student();
    ArrayList<Student> students = new ArrayList<Student>();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String id = request.getParameter("id");
            String option = request.getParameter("option");
            
            switch (option) {
                case "1":
                    // Registrar info
                    //student.setId(Integer.parseInt(request.getParameter("valorCtx")));
                    student.setName(request.getParameter("nameCtx"));
                    student.setLastName(request.getParameter("lastnameCtx"));
                    student.setBirthday(request.getParameter("birthdayCtx"));
                    studentDao.insertStudent(student);
                    out.println("<script>generalAjax(0,4);</script>");
                    break;
            // Modificar
                case "2":
                    student.setId(Integer.parseInt(request.getParameter("id")));
                    student.setName(request.getParameter("nameCtx"));
                    student.setLastName(request.getParameter("lastnameCtx"));
                    student.setBirthday(request.getParameter("birthdayCtx"));
                    //student.updateStudent(preguntaDto);
                    
                    out.println("<script>alert('Se modifico correctamente');</script>");
                    out.println("<script>ressetControls();generalAjax(0,4);</script>");
                    
                    //setDataInControls(enunciado,valor)
                    
                    break;
            // Eliminar
                case "3":
                    student.setId(Integer.parseInt(request.getParameter("id")));
                    //studentDao.delete(student);
                    out.println("<script>generalAjax(0,4);</script>");
                    out.println("<script>alert('Se ha eliminado el registro nro:'+"+student.getId()+");</script>");
                    break;
                case "4":
                    // Consultar
                    
                    students = studentDao.getStudents();
                    if(students.size()>0){
                        generateTbl(out);
                    }else{
                        out.println("<h1>no tenemos datos almacenados</h1>");
                    }   break;
                default:
                    break;
            }
            
        }
    }
    
    private void generateTbl(PrintWriter out){
        
        String htmlTbl = "<thead>" +
                            "<tr>" +
                            "<th>Id</th>" +
                            "<th>Enunciado</th>" +
                            "<th>Valor</th>" +
                            "<th>Modificar</th>" +
                            "<th>Eliminar</th>" +
                            "</tr>" +
                        "</thead>";
        
        for (int i = 0; i < students.size(); i++) {
            htmlTbl += "<tr>";
            htmlTbl += "<td>"+students.get(i).getId()+"</td>";
            htmlTbl += "<td>"+students.get(i).getName()+"</td>";
            htmlTbl += "<td>"+students.get(i).getLastName()+"</td>";
            
            htmlTbl += "<td><button id=\"modificarBtn\" name=\"modificarBtn\" type=\"button\" class=\"btn btn-warning\" aria-label=\"Left Align\" onClick=\"setDataInControls("+students.get(i).getId()+",'"+students.get(i).getName()+"','"+students.get(i).getLastName()+"')\">";
            htmlTbl += "<span class=\"glyphicon glyphicon-copy\" aria-hidden=\"true\"></span>";
            htmlTbl += "</button></td>";

            htmlTbl += "<td><button id=\"deleteBtn\" name=\"deleteBtn\" type=\"button\" class=\"btn btn-danger\" aria-label=\"Left Align\" onClick=\"generalAjax("+students.get(i).getId()+",3)\">";
            htmlTbl += "<span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span>";
            htmlTbl += "</button></td>";
            
            htmlTbl += "</tr>";
        }
        out.println(htmlTbl);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        String nameCtx = request.getParameter("nameCtx");
        String lastNameCtx = request.getParameter("lastNameCtx");
        String birthdayCtx = request.getParameter("birthdayCtx");
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
